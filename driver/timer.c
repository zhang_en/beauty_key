#include "timer.h"


void timer_base_set(u16 per,u16 psc)
{
	timer_parameter_struct base_timer_structure;
	
	rcu_periph_clock_enable(RCU_TIMER6);
	
	base_timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	base_timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	base_timer_structure.counterdirection=TIMER_COUNTER_UP;
	base_timer_structure.period=per;
	base_timer_structure.prescaler=psc;
	base_timer_structure.repetitioncounter=0;
	
	timer_init(TIMER6,&base_timer_structure);
	
	timer_interrupt_enable(TIMER6,TIMER_INT_UP);
	nvic_irq_enable(TIMER6_IRQn,2,2);
	
	timer_enable(TIMER6);
}

